require 'rails_helper'

RSpec.describe "routes for home", type: :routing do
    describe 'home ページへのルーティングのテスト' do
        it 'homeアクションのルーティング' do
            expect(get("/static_pages/home")).to route_to("static_pages#home")
        end
    end
    describe 'help ページへのルーティングのテスト' do
        it 'helpアクションのルーティング' do
            expect(get("/static_pages/help")).to route_to("static_pages#help")
        end
    end
    describe 'about ページへのルーティングのテスト' do
        it 'aboutアクションのルーティング' do
            expect(get("/static_pages/about")).to route_to("static_pages#about")
        end
    end
    describe 'contact ページへのルーティングのテスト' do
        it 'contactアクションのルーティング' do
            expect(get("/static_pages/contact")).to route_to("static_pages#contact")
        end
    end
end