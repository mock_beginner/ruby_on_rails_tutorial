require 'rails_helper'

RSpec.describe "StaticPages", type: :request do
  describe "GET /static_pages/home" do
    it "HTTPレスポンスが200 OKになる" do
      get static_pages_home_url
      expect(response).to have_http_status(200)
    end
  end
  describe "GET /static_pages/help" do
    it "HTTPレスポンスが200 OKになる" do
      get static_pages_help_url
      expect(response).to have_http_status(200)
    end
  end
  describe "GET /static_pages/about" do
    it "HTTPレスポンスが200 OKになる" do
      get static_pages_about_url
      expect(response).to have_http_status(200)
    end
  end
  describe "GET /static_pages/contact" do
    it "HTTPレスポンスが200 OKになる" do
      get static_pages_contact_url
      expect(response).to have_http_status(200)
    end
  end
end
