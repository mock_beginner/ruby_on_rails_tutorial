require 'rails_helper'

RSpec.describe "static_pages/about", type: :view do
  describe 'about.html.erbのテスト' do
    it 'h1タグ内にAboutが表示されているか' do
      visit static_pages_about_url
      expect(page).to have_selector('h1', text: 'About')
    end
    it 'pタグ内にRuby on Rails Tutorial is a book and screencast to teach web development with This is the sample application for the tutorial. が表示されているか' do
      visit static_pages_about_url
      expect(page).to have_selector('p', text: 'Ruby on Rails Tutorial is a book and screencast to teach web development with Ruby on Rails. This is the sample application for the tutorial.')
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/が含まれているか' do
      visit static_pages_about_url
      expect(page).to have_link 'Ruby on Rails Tutorial', href: 'https://railstutorial.jp/'
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/#ebookが含まれているか' do
      visit static_pages_about_url
      expect(page).to have_link 'book', href: 'https://railstutorial.jp/#ebook'
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/#screencastが含まれているか' do
      visit static_pages_about_url
      expect(page).to have_link 'screencast', href: 'htps://railstutorial.jp/#screencast' 
    end
    it 'aタグ内にリンクhttps://ubyonrails.org/が含まれているか' do
      visit static_pages_about_url
      expect(page).to have_link 'Ruby on Rails', href: 'https://rubyonrails.org/'
    end
  end
end
