require 'rails_helper'

RSpec.describe "static_pages/contact", type: :view do
  describe 'contact.html.erbのテスト' do
    it 'h1タグ内にContactが表示されているか' do
      visit static_pages_contact_url
      expect(page).to have_selector('h1', text: 'Contact')
    end
    it 'pタグ内にContact the Ruby on Rails Tutorial about the sample app at the contact page.が表示されているか' do
      visit static_pages_contact_url
      expect(page).to have_selector('p', text: 'Contact the Ruby on Rails Tutorial about the sample app at the contact page.')
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/contactが含まれているか' do
      visit static_pages_contact_url
      expect(page).to have_link 'contact page', href: 'https://railstutorial.jp/contact'
    end
  end
end
