require 'rails_helper'

RSpec.describe "static_pages/help", type: :view do
  describe 'help.html.erbのテスト' do
    it 'h1タグ内にHelpが表示されているか' do
      visit static_pages_help_url
      expect(page).to have_selector('h1', text: 'Help')
  end
    it 'pタグ内にGet help on the Ruby on Rails Tutorial at the Rails Tutorial help page.
    To get Help on this sample app, see the Ruby on Rails Tutorial book.が表示されているか' do
      visit static_pages_help_url
      expect(page).to have_selector('p', text: 'Get help on the Ruby on Rails Tutorial at the Rails Tutorial help page.To get Help on this sample app, see the Ruby on Rails Tutorial book.')
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/helpが含まれているか' do
      visit static_pages_help_url
      expect(page).to have_link 'Rails Tutorial help page', href: 'https://railstutorial.jp/help'
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/#ebookが含まれているか' do
      visit static_pages_help_url
      expect(page).to have_link 'Ruby on Rails Tutorial book', href: 'https://railstutorial.jp/#ebook'
    end
    it 'emタグ内にRuby on Rails Tutorialが含まれているか' do
      visit static_pages_help_url
      expect(page).to have_selector('em', text: 'Ruby on Rails Tutorial')
    end
  end
end
