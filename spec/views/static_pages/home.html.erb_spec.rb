require 'rails_helper'

RSpec.describe "static_pages/home", type: :view do
  describe 'home.html.erbのテスト' do
    it 'h1タグ内にSample Appが表示されているか' do
      visit static_pages_home_url
      expect(page).to have_selector('h1', text: 'Welcome to the Sample App')
    end
    it 'h2タグ内にThis is the home page for the Ruby on Rails Tutorial sample application.が表示されているか' do
      visit static_pages_home_url
      expect(page).to have_selector('h2', text: 'This is the home page for the Ruby on Rails Tutorial Sample application.')
    end
    it 'aタグ内にリンクhttps://railstutorial.jp/が含まれているか' do
      visit static_pages_home_url
      expect(page).to have_link 'the Ruby on Rails', href: 'https://railstutorial.jp/'
    end
  end
end
