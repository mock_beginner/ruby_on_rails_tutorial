import Aurelia from 'aurelia';
import { MyApp } from './my-app';
// import pages
//import * as pagesComponents from './pages/page-module';
// import routing
import { RouterConfiguration } from 'aurelia';

Aurelia
  .register(RouterConfiguration)
  .app(MyApp)
  .start();
