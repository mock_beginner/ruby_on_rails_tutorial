// regist pages
export * from './about/about-page';
export * from './help/help-page';
export * from './home/home-page';
export * from './login/login-page';
export * from './content/content-page';
export * from './news/news-page';